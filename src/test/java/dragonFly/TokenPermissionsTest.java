package dragonFly;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import dragonFly.common.Token;
import dragonFly.configs.TestCommonConfig;
import dragonFly.errors.DragonFlyException;
import dragonFly.services.TokenPermissionsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = {TestCommonConfig.class})
@SpringBootTest
@ActiveProfiles("test")
@EnableAutoConfiguration
public class TokenPermissionsTest {

  @Autowired
  private TokenPermissionsService tokenPermissionsService;

  @Test
  public void isTokenExistForMissingTokenTest() {
    Token absentToken = new Token("absentToken");
    assertThat(tokenPermissionsService.isTokenExist(absentToken), is("Token doesn't exist"));
  }

  @Test
  public void isTokenExistForTestTokenTest() {
    Token absentToken = new Token("testToken1");
    assertNull(tokenPermissionsService.isTokenExist(absentToken));
  }

  @Test
  public void isTokenActiveForTestTokenTest() {
    Token absentToken = new Token("testToken1");
    assertNull(tokenPermissionsService.isTokenActive(absentToken));
  }

  @Test
  public void isTokenActiveForDisableTokenTest() {
    Token absentToken = new Token("disableToken");
    assertThat(tokenPermissionsService.isTokenActive(absentToken), is("Token is disable"));
  }

  @Test
  public void validateForMissingTokenTest() {
    Token absentToken = new Token("absentToken");
    DragonFlyException dragonFlyException = assertThrows(DragonFlyException.class,
        () -> tokenPermissionsService.validateToken(absentToken));
    assertThat(dragonFlyException.getMessage(),is("Token doesn't exist"));
  }

  @Test
  public void validateForDisableTokenTest() {
    Token absentToken = new Token("disableToken");
    DragonFlyException dragonFlyException = assertThrows(DragonFlyException.class,
        () -> tokenPermissionsService.validateToken(absentToken));
    assertThat(dragonFlyException.getMessage(),is("Token is disable"));
  }
}
