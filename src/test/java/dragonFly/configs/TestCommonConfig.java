package dragonFly.configs;

import dragonFly.common.TokenStatus;
import dragonFly.entities.TokenEntity;
import dragonFly.repositories.DashboardPermissionRepository;
import dragonFly.repositories.DragonFlyDashboardPermissionRepository;
import dragonFly.repositories.DragonFlyPermissionRepository;
import dragonFly.repositories.DragonFlyTokenRepository;
import dragonFly.repositories.PermissionRepository;
import dragonFly.repositories.TokenRepository;
import dragonFly.services.TokenPermissionsService;
import java.sql.Timestamp;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
public class TestCommonConfig {

  @Bean
  public TokenPermissionsService getTokenPermissionService() {
    TokenRepository tokenRepository = Mockito.mock(TokenRepository.class);
    Mockito.when(tokenRepository.findByToken("absentToken")).thenReturn(null);
    Mockito.when(tokenRepository.findByToken("testToken1")).thenReturn(
        new TokenEntity("testToken1", new Timestamp(1638708554), new Timestamp(1638708554),
            TokenStatus.ACTIVE.name()));
    Mockito.when(tokenRepository.findByToken("disableToken"))
        .thenReturn(new TokenEntity("disableToken", new Timestamp(1638708554),
            new Timestamp(1638708554), TokenStatus.DISABLE.name()));

    PermissionRepository permissionRepository = Mockito.mock(PermissionRepository.class);

    DashboardPermissionRepository dashboardPermissionRepository = Mockito
        .mock(DashboardPermissionRepository.class);

    return new TokenPermissionsService(new DragonFlyTokenRepository(tokenRepository),
        new DragonFlyPermissionRepository(permissionRepository),
        new DragonFlyDashboardPermissionRepository(dashboardPermissionRepository));
  }
}
