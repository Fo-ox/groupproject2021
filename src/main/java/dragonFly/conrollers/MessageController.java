package dragonFly.conrollers;

import dragonFly.entities.Message;
import dragonFly.services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private final MessageService messageService;

    @RequestMapping(method = GET, path = "/getLastMessageByChatId")
    public Message getLastMessageByChatId(@RequestParam String chatId) {
        return messageService.getLastMessageByChatId(chatId);
    }

    //getAllByChat

    @RequestMapping(method = GET, path = "/getMessagesByChatId")
    public List<Message> getMessagesByChatId(@RequestParam String chatId) {
        return messageService.getAllMessageByChat(chatId);
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    @RequestMapping(method = GET, path = "/sendMessagesByChatId")
    public List<Message> sendMessage(@Payload Message chatMessage) {
        messageService.createMessage(chatMessage);
        return messageService.getAllMessageByChat(chatMessage.getChatId());
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public Message addUser(@Payload Message chatMessage,
                           SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("fromUserId", chatMessage.getFromUserId());
        headerAccessor.getSessionAttributes().put("chatId", chatMessage.getChatId());
        return chatMessage;
    }
}
