package dragonFly.conrollers;

import dragonFly.entities.Chat;
import dragonFly.entities.Message;
import dragonFly.entities.User;
import dragonFly.requests.ChatCredentials;
import dragonFly.services.ChatsService;
import dragonFly.services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/chats")
public class ChatsController {

    @Autowired
    private final ChatsService chatsService;

    @RequestMapping(method = GET, path = "/getAllChatsUser")
    public List<Chat> getAllChatsUser(@RequestParam String userId) {
        return chatsService.getALLChatsUser(userId);
    }

    @RequestMapping(method = POST, path = "/create")
    public String createChat(@RequestBody ChatCredentials chatCredentials) {
        return chatsService.createChat(chatCredentials.getTitle(), chatCredentials.getUsers(), chatCredentials.getUserId());
    }

    @RequestMapping(method = GET, path = "/getChatId")
    public Chat getChatId(@RequestParam String chatId) {
        return chatsService.getChatById(chatId);
    }

    @RequestMapping(method = PATCH, path = "/addUsersToChatById")
    public Chat addUsersToChatById(@RequestBody ChatCredentials chatCredentials) {
        return chatsService.addUsersToChatById(chatCredentials.getChat_id(), chatCredentials.getUserId());
    }

    @MessageMapping("/user.sendChat")
    @SendTo("/topic/chat")
    @RequestMapping(method = GET, path = "/sendChat")
    public List<Chat> sendChat(@Payload ChatCredentials chatCredentials) {
        chatsService.createChat(chatCredentials.getTitle(), chatCredentials.getUsers(), chatCredentials.getUserId());
        return chatsService.getALLChatsUser(chatCredentials.getUserId());
    }
}
