package dragonFly.conrollers;

import dragonFly.entities.Comment;
import dragonFly.requests.CommentCredentials;
import dragonFly.services.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private final CommentService commentService;

    @RequestMapping(method = POST, path = "/create")
    public Comment createComment(@RequestBody CommentCredentials commentCredentials) {
        return commentService.createComment(commentCredentials.getCommentText(), commentCredentials.getUserId(), commentCredentials.getTaskId());
    }

    @RequestMapping(method = GET, path = "/getCommentsByTaskId")
    public List<Comment> getCommentsByTaskId(@RequestParam String taskId) {
        return commentService.getCommentsByTaskId(taskId);
    }
}
