package dragonFly.conrollers;

import dragonFly.entities.Dashboard;
import dragonFly.requests.DashboardManifest;
import dragonFly.requests.DashboardUpdateManifest;
import dragonFly.services.DashboardService;
import dragonFly.services.TokenPermissionsService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/dashboards")
public class DashboardController {

  private DashboardService dashboardService;

  @RequestMapping(method = RequestMethod.POST, path = "/create")
  public Dashboard create(@RequestParam String token,
      @RequestBody DashboardManifest dashboardManifest) {
    return dashboardService.create(token,dashboardManifest);
  }

  @RequestMapping(method = RequestMethod.GET, path = "/getAll")
  public List<Dashboard> getAll() {
    return dashboardService.getAll();
  }

  @RequestMapping(method = RequestMethod.GET, path = "/find")
  public Dashboard find(@RequestParam String dashboardId) {
    return dashboardService.find(dashboardId);
  }

  @RequestMapping(method = RequestMethod.POST , path = "/update")
  public Dashboard update(@RequestParam String token,@RequestBody DashboardUpdateManifest dashboardUpdateManifest){
    return dashboardService.update(token,dashboardUpdateManifest);
  }
}
