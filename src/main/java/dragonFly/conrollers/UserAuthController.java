package dragonFly.conrollers;

import dragonFly.entities.UserAuth;
import dragonFly.requests.UserAuthCredentials;
import dragonFly.services.UserAuthService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/userAuth")
public class UserAuthController {

    @Autowired
    private final UserAuthService userAuthService;

    @RequestMapping(method = GET,path = "/getAll")
    public List<UserAuth> getAll() {
        return userAuthService.getAllUsers();
    }

    @RequestMapping(method = POST,path = "/add")
    public String addUser(@RequestBody UserAuthCredentials userCredentials){
        return userAuthService.createUser(userCredentials.getUserId(),userCredentials.getLogin(),userCredentials.getPassword());
    }
}
