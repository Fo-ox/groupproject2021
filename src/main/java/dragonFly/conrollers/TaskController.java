package dragonFly.conrollers;

import dragonFly.entities.Task;
import dragonFly.requests.TaskManifest;
import dragonFly.requests.TaskUpdateManifest;
import dragonFly.services.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/tasks")
public class TaskController {

    private TaskService taskService;

    @RequestMapping(method = RequestMethod.POST,path = "/create")
    public Task create(@RequestParam String token,@RequestBody TaskManifest taskManifest){
        return taskService.create(token,taskManifest);
    }

    @RequestMapping(method = RequestMethod.POST,path = "/update")
    public Task update(@RequestParam String token,@RequestBody TaskUpdateManifest taskUpdateManifest){
        return taskService.update(token,taskUpdateManifest);
    }

    @RequestMapping(method = RequestMethod.GET,path = "/getAll")
    public List<Task> getAll(){
       return taskService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET,path = "/find")
    public Task find(@RequestParam String taskId){
        return taskService.find(taskId);
    }

    @RequestMapping(method = RequestMethod.GET,path = "/findByDashboardId")
    public List<Task> findByDashboardId(@RequestParam String dashboardId){
        return taskService.getTasksByDashboardId(dashboardId);
    }
}
