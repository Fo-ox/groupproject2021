package dragonFly.conrollers;

import dragonFly.entities.TokenEntity;
import dragonFly.services.AdminTokenService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/token")
public class AdminTokenController {

    @Autowired
    private AdminTokenService tokenService;

    @RequestMapping(method = RequestMethod.POST, path = "/createAdmin")
    public TokenEntity create() {
        return tokenService.createToken();
    }
}
