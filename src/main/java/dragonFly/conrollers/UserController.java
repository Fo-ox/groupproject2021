package dragonFly.conrollers;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import dragonFly.dto.UserDto;
import dragonFly.entities.User;
import dragonFly.requests.UserCredentials;
import dragonFly.services.UserService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@AllArgsConstructor
@Controller
@ResponseBody
@RequestMapping("/user")
public class UserController {

  @Autowired
  private final UserService userService;

  @RequestMapping(method = GET, path = "/getAll")
  public List<UserDto> getAll() {
    return userService.getAllUsers();
  }

  @RequestMapping(method = POST, path = "/create")
  public UserDto createUser(@RequestParam String token,
      @RequestBody UserCredentials userCredentials) {
    return userService.createUser(token, userCredentials.getLogin(), userCredentials.getPassword(),
        userCredentials.getFirstName(), userCredentials.getLastName(), userCredentials.getProfileIcon());
  }

  @RequestMapping(method = GET, path = "/login")
  public User login(@RequestParam String login, @RequestParam String password) {
    return userService.login(login, password);
  }

  @RequestMapping(method = GET, path = "/disable")
  public String disable(@RequestParam String userId) {
    return userService.disable(userId);
  }

  @RequestMapping(method = GET, path = "/getUserByUserToken")
  public UserDto getUserByUserToken(@RequestParam String userToken) {
    return userService.getUserByUserToken(userToken);
  }
}