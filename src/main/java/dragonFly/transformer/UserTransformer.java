package dragonFly.transformer;

import dragonFly.dto.UserDto;
import dragonFly.entities.User;
import org.springframework.stereotype.Service;

@Service
public class UserTransformer {

    public UserDto transform(User user) {
        UserDto userDto = new UserDto();
        userDto.setUserId(user.getUserId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setProfileIcon(user.getProfileIcon());
        return userDto;
    }
}
