package dragonFly.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class WrongCredentials extends ResponseStatusException {
    public WrongCredentials() {
        super(HttpStatus.BAD_REQUEST,"Wrong credentials");
    }
}
