package dragonFly.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UserWithLoginAlreadyExistException extends ResponseStatusException {
    public UserWithLoginAlreadyExistException(){
        super(HttpStatus.BAD_REQUEST,"User with same login already exist");
    }
}
