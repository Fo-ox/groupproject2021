package dragonFly.errors;

public class DragonFlyException extends RuntimeException {

    public DragonFlyException(String message,Throwable cause){
        super(message,cause);
    }

    public DragonFlyException(String message){
        super(message);
    }

}
