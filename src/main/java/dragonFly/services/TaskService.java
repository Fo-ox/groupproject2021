package dragonFly.services;

import dragonFly.common.TaskPriority;
import dragonFly.common.TaskStatus;
import dragonFly.common.Token;
import dragonFly.entities.Task;
import dragonFly.repositories.TaskRepository;
import dragonFly.requests.TaskManifest;
import dragonFly.requests.TaskUpdateManifest;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public class TaskService {

  private final TaskRepository taskRepository;
  private final DashboardService dashboardService;
  private final TokenPermissionsService tokenPermissionsService;

  public Task create(String token, TaskManifest taskManifest) {

    String dashboardId = taskManifest.getDashboardId();
    tokenPermissionsService
        .validateToken(new Token(token).shouldHaveCreateTaskPermissions(dashboardId));

    String taskId = String.format("task-%s", UUID.randomUUID().toString());
    String description = Optional.ofNullable(taskManifest.getDescription())
        .orElse("This section should contains a task description");
    Date date = new Date();
    String assignedBy = Optional.ofNullable(taskManifest.getAssignedBy())
        .orElseGet(() -> dashboardService.getDefaultAssignedBy(dashboardId));
    String status = Optional.ofNullable(taskManifest.getStatus()).orElse(TaskStatus.OPEN.name());
    String priority = Optional.ofNullable(taskManifest.getPriority())
        .orElse(TaskPriority.NORMAL.name());
    return taskRepository.save(new Task(taskId, dashboardId, taskManifest.getTitle(), description,
        taskManifest.getCreatedBy(), assignedBy, status, priority, taskManifest.getType(), date,
        date));
  }

  public Task update(String token, TaskUpdateManifest taskUpdateManifest) {

    @NonNull String dashboardId = taskUpdateManifest.getDashboardId();
    tokenPermissionsService
        .validateToken(new Token(token).shouldHaveUpdateTaskPermissions(dashboardId));

    @NonNull String taskId = taskUpdateManifest.getTaskId();
    Optional.ofNullable(taskUpdateManifest.getTitle())
        .ifPresent(title -> taskRepository.updateTitle(taskId, title));
    Optional.ofNullable(taskUpdateManifest.getDescription())
        .ifPresent(description -> taskRepository.updateDescription(taskId, description));
    Optional.ofNullable(taskUpdateManifest.getAssignedBy())
        .ifPresent(assignedBy -> taskRepository.updateAssignedBy(taskId, assignedBy));
    Optional.ofNullable(taskUpdateManifest.getStatus())
        .ifPresent(status -> taskRepository.updateStatus(taskId, status));
    Optional.ofNullable(taskUpdateManifest.getPriority())
        .ifPresent(priority -> taskRepository.updatePriority(taskId, priority));
    Optional.ofNullable(taskUpdateManifest.getType())
        .ifPresent(type -> taskRepository.updateType(taskId, type));
    Date date = new Date();
    taskRepository.updateModificationDate(taskId, date);
    return taskRepository.findByTaskId(taskId);
  }

  public List<Task> getAll() {
    return taskRepository.findAll();
  }

  public List<Task> getTasksByDashboardId(String dashboardId) {
    return taskRepository.findByDashboardId(dashboardId);
  }

  public Task find(String taskId) {
    return taskRepository.findByTaskId(taskId);
  }
}
