package dragonFly.services;

import dragonFly.repositories.DashboardPermissionRepository;
import dragonFly.repositories.PermissionRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PermissionService {

    private final PermissionRepository permissionRepository;
    private final DashboardPermissionRepository dashboardPermissionRepository;

    public Boolean isTokenExist(String token){
        return permissionRepository.findByToken(token) != null;
    }
}
