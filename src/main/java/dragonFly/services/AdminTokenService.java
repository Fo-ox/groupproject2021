package dragonFly.services;

import dragonFly.common.TokenStatus;
import dragonFly.entities.PermissionEntity;
import dragonFly.entities.TokenEntity;
import dragonFly.repositories.PermissionRepository;
import dragonFly.repositories.TokenRepository;
import liquibase.pro.packaged.A;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

@AllArgsConstructor
@Service
public class AdminTokenService {

    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private PermissionRepository permissionRepository;

   public TokenEntity createToken(){
        String userToken = String.format("token-%s", UUID.randomUUID());

        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR,calendar.get(Calendar.YEAR) + 1);
        Date date = calendar.getTime();

        permissionRepository.save(new PermissionEntity(userToken,true,true));

        return tokenRepository.save(
                new TokenEntity(userToken, new Date(),
                        date,
                        TokenStatus.ACTIVE.name()));
    }

}
