package dragonFly.services;


import dragonFly.entities.Chat;
import dragonFly.entities.Comment;
import dragonFly.repositories.CommentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class CommentService {
    private final CommentRepository commentRepository;

    public Comment createComment(String commentText, String userId,String taskId) {
        Comment comment = new Comment();
        comment.setCommentId(String.valueOf(UUID.randomUUID()));
        comment.setCommentText(commentText);
        comment.setCommentTime(new Date());
        comment.setUserId(userId);
        comment.setTaskId(taskId);
        return commentRepository.save(comment);
    }

    public List<Comment> getCommentsByTaskId(String taskId) {
        return commentRepository.findByTaskId(taskId);
    }
}
