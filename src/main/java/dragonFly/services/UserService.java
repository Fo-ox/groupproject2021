package dragonFly.services;

import dragonFly.common.Token;
import dragonFly.common.TokenStatus;
import dragonFly.common.UserStatus;
import dragonFly.dto.UserDto;
import dragonFly.entities.PermissionEntity;
import dragonFly.entities.TokenEntity;
import dragonFly.entities.User;
import dragonFly.errors.DragonFlyException;
import dragonFly.errors.UserWithLoginAlreadyExistException;
import dragonFly.errors.WrongCredentials;
import dragonFly.repositories.DragonFlyTokenRepository;
import dragonFly.repositories.PermissionRepository;
import dragonFly.repositories.UserRepository;
import java.util.*;
import java.util.stream.Collectors;

import dragonFly.transformer.UserTransformer;
import liquibase.pro.packaged.S;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private DragonFlyTokenRepository tokenRepository;
  @Autowired
  private PermissionRepository permissionRepository;
  @Autowired
  private TokenPermissionsService tokenPermissionsService;
  @Autowired
  private UserTransformer userTransformer;

  public List<UserDto> getAllUsers() {
    return userRepository.findAll().stream()
            .map(user -> userTransformer.transform(user))
            .collect(Collectors.toList());
  }

  public UserDto createUser(String token, String login, String password, String firstName,
      String lastName, String profileIcon) {

    tokenPermissionsService.validateToken(new Token(token).shouldHaveCreateUsersPermissions());

    Optional.ofNullable(userRepository.findByLogin(login))
        .ifPresent(v -> new UserWithLoginAlreadyExistException());

    String userToken = String.format("token-%s", UUID.randomUUID());
    String userId = String.format("user-%s", UUID.randomUUID());
    userRepository.save(new User(userId, login, password, userToken,firstName,lastName, profileIcon))
        .getUserToken();

    Calendar calendar = new GregorianCalendar();
    calendar.set(Calendar.YEAR,calendar.get(Calendar.YEAR) + 1);
    Date date = calendar.getTime();

    tokenRepository.save(
        new TokenEntity(userToken, new Date(),
                date,
            TokenStatus.ACTIVE.name()));
    permissionRepository.save(new PermissionEntity(userToken, false, false));
    return userTransformer.transform(userRepository.findByUserToken(userToken));
  }

  public User login(String login, String password) {
    User user = userRepository.findByLoginAndPassword(login, password);
    return Optional.ofNullable(user).map(v -> {
      UserStatus userStatus = UserStatus.valueOf(v.getStatus());
      if (userStatus.equals(UserStatus.INACTIVE)) {
        throw new DragonFlyException("User is inactive");
      } else {
        return v;
      }
    }).orElseThrow(WrongCredentials::new);
  }

  public String disable(String userId) {
    User user = userRepository.findByUserId(userId);
    return Optional.ofNullable(user).map(v -> userRepository.save(
        new User(v.getUserId(), v.getLogin(), v.getPassword(), v.getUserToken(),
            UserStatus.INACTIVE.name(), v.getFirstName(), v.getLastName(), v.getProfileIcon()))
        .getUserId()).orElseThrow(WrongCredentials::new);
  }

  public UserDto getUserByUserToken(String userToken){
    return userTransformer.transform(userRepository.findByUserToken(userToken));
  }


}
