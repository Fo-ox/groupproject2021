package dragonFly.services;

import dragonFly.entities.Chat;
import dragonFly.entities.User;
import dragonFly.repositories.ChatsRepository;
import dragonFly.repositories.UserRepository;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ChatsService {

  private final ChatsRepository chatsUserRepository;
  private final UserRepository userRepository;

  public String createChat(String title, List<User> users,String userId) {
    Chat chart = new Chat(title);
    chart.setChartId(String.valueOf(UUID.randomUUID()));
    chart.addUsers(users);
    chart.setCreatedBy(userId);
    return chatsUserRepository.save(chart).getTitleChat();
  }

  public Chat getChatById(String chatId) {
    return chatsUserRepository.findChartByTitleChatId(chatId);
  }

  public Chat addUsersToChatById(String chatId, String userId) {
    Chat chat = getChatById(chatId);
    chat.addUsers(Collections.singletonList(userRepository.findByUserId(userId)));
    return chatsUserRepository.save(chat);
  }

  public List<Chat> getALLChatsUser(String userId) {
    List<Chat> chats = chatsUserRepository.findAll();
    chats.removeIf(chat -> !chat.getUsers().stream().anyMatch(user -> user.getUserId()
        .equals(userId)));
    return chats;
  }
}
