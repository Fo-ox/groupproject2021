package dragonFly.services;

import dragonFly.entities.Message;
import dragonFly.repositories.MessageRepository;
import liquibase.pro.packaged.S;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class MessageService {
    private final MessageRepository messageRepository;

    public Message createMessage(Message message) {
        Message messageSave = new Message();
        messageSave.setMessageTime(new Date());
        messageSave.setMessageId(UUID.randomUUID().toString());
        messageSave.setFromUserId(message.getFromUserId());
        messageSave.setMessageText(message.getMessageText());
        messageSave.setChatId(message.getChatId());
        return messageRepository.save(messageSave);
    }

    public List<Message> getAllMessageByChat(String chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    public Message getLastMessageByChatId(String chatId) {
        return messageRepository.findMessageByChatId(chatId);
    }
}
