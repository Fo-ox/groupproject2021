package dragonFly.services;

import dragonFly.common.Token;
import dragonFly.common.TokenStatus;
import dragonFly.entities.DashboardPermissionEntity;
import dragonFly.entities.PermissionEntity;
import dragonFly.entities.TokenEntity;
import dragonFly.errors.DragonFlyException;
import dragonFly.repositories.DragonFlyDashboardPermissionRepository;
import dragonFly.repositories.DragonFlyPermissionRepository;
import dragonFly.repositories.DragonFlyTokenRepository;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class TokenPermissionsService {

  private final DragonFlyTokenRepository tokenRepository;
  private final DragonFlyPermissionRepository permissionRepository;
  private final DragonFlyDashboardPermissionRepository dashboardPermissionRepository;
  private final Map<String, Optional<TokenEntity>> tokenEntityMap = new HashMap<>();
  private final Map<String, Optional<PermissionEntity>> permissionEntityMap = new HashMap<>();
  private final Map<String, Map<String, Optional<DashboardPermissionEntity>>> dashboardPermissionEntityMap = new HashMap<>();

  public TokenPermissionsService(DragonFlyTokenRepository tokenRepository,
      DragonFlyPermissionRepository permissionRepository,
      DragonFlyDashboardPermissionRepository dashboardPermissionRepository) {
    this.tokenRepository = tokenRepository;
    this.permissionRepository = permissionRepository;
    this.dashboardPermissionRepository = dashboardPermissionRepository;
  }

  public void validateToken(Token token) {
    for (Iterator<Function<TokenPermissionsService, String>> iterator = token.getPermissionChecks()
        .iterator();
        iterator.hasNext(); ) {
      Function<TokenPermissionsService, String> element = iterator.next();
      Optional.ofNullable(element.apply(this)).ifPresent(v -> {
        throw new DragonFlyException(v);
      });
    }
  }

  public String isTokenExist(Token token) {
    Optional<TokenEntity> tokenEntity = getTokenEntity(token.getTokenId());
    return tokenEntity.isPresent() ? null : "Token doesn't exist";
  }

  public String isTokenActive(Token token) {
    TokenEntity tokenEntity = getTokenEntity(token.getTokenId()).get();
    return tokenEntity.getStatus().equals(TokenStatus.DISABLE.name()) ? "Token is disable" : null;
  }

  public String isHaveCreateUsersPermissions(Token token) {
    return getPermissionEntity(token.getTokenId()).get().getIsAllowCreateUsers() ? null
        : "Token doesn't have permissions to create users";
  }

  public String isHaveCreateDashboardsPermissions(Token token) {
    return getPermissionEntity(token.getTokenId()).get().getIsAllowCreateDashboards() ? null
        : "Token doesn't have permissions to create dashboards";
  }

  public String isHaveCreateTasksPermissions(Token token, String dashboardId) {
    Optional<DashboardPermissionEntity> dashboardPermissionEntity = getDashboardPermissionEntity(
        token.getTokenId(), dashboardId);
    if (dashboardPermissionEntity.isPresent()) {
      return dashboardPermissionEntity.get().getIsAllowCreateTasks() ? null
          : "Token doesn't have permissions to create tasks";
    } else {
      return "Token doesn't have permissions to create tasks";
    }
  }

  public String isHaveUpdateTasksPermissions(Token token, String dashboardId) {
    Optional<DashboardPermissionEntity> dashboardPermissionEntity = getDashboardPermissionEntity(
        token.getTokenId(), dashboardId);
    if (dashboardPermissionEntity.isPresent()) {
      return dashboardPermissionEntity.get().getIsAllowUpdateTasks() ? null
          : "Token doesn't have permissions to update tasks";
    } else {
      return "Token doesn't have permissions to update tasks";
    }
  }

  private Optional<TokenEntity> getTokenEntity(String tokenId) {
    return tokenEntityMap.computeIfAbsent(tokenId, tokenRepository::findByToken);
  }

  private Optional<PermissionEntity> getPermissionEntity(String tokenId) {
    return permissionEntityMap.computeIfAbsent(tokenId, permissionRepository::findByToken);
  }

  private Optional<DashboardPermissionEntity> getDashboardPermissionEntity(String tokenId,
      String dashboardId) {
    if (dashboardPermissionEntityMap.containsKey(tokenId)) {
      return dashboardPermissionEntityMap.get(tokenId).computeIfAbsent(dashboardId,
          (v) -> dashboardPermissionRepository.findByTokenAndDashboardId(tokenId, v));
    } else {
      HashMap<String, Optional<DashboardPermissionEntity>> dashboardPermissionEntityMap = new HashMap<>();
      dashboardPermissionEntityMap.put(dashboardId,
          dashboardPermissionRepository.findByTokenAndDashboardId(tokenId, dashboardId));
      this.dashboardPermissionEntityMap.put(tokenId, dashboardPermissionEntityMap);
      return this.dashboardPermissionEntityMap.get(tokenId).get(dashboardId);
    }
  }

  public String isHaveUpdateDashboardPermission(Token token, String dashboardId) {
    Optional<DashboardPermissionEntity> dashboardPermissionEntity = getDashboardPermissionEntity(
        token.getTokenId(), dashboardId);
    if (dashboardPermissionEntity.isPresent()) {
      return dashboardPermissionEntity.get().getIsAllowUpdateDashboard() ? null
          : "Token doesn't have permissions to update dashboard";
    } else {
      return "Token doesn't have permissions to update dashboard";
    }
  }
}
