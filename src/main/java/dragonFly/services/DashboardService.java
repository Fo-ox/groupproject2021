package dragonFly.services;

import dragonFly.common.Token;
import dragonFly.entities.Dashboard;
import dragonFly.entities.DashboardPermissionEntity;
import dragonFly.entities.User;
import dragonFly.repositories.DashboardRepository;
import dragonFly.repositories.DragonFlyDashboardPermissionRepository;
import dragonFly.repositories.UserRepository;
import dragonFly.requests.DashboardManifest;
import dragonFly.requests.DashboardUpdateManifest;

import java.util.*;

import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public class DashboardService {

  private final DashboardRepository dashboardRepository;
  private final UserRepository userRepository;
  private final TokenPermissionsService tokenPermissionsService;
  private final DragonFlyDashboardPermissionRepository dashboardPermissionRepository;

  public Dashboard create(String token, DashboardManifest dashboardManifest) {

    tokenPermissionsService.validateToken(new Token(token).shouldHaveCreateDashboardPermissions());

    String dashboardId = String
        .format("dashboard-%s", UUID.randomUUID().toString().substring(0, 34));
    String description = Optional.ofNullable(dashboardManifest.getDescription())
        .orElse("This section should contains a dashboard description");
    Optional.ofNullable(dashboardManifest.getTeam()).ifPresent(v -> v.stream().map(
        userRepository::findByUserId).map(user -> dashboardPermissionRepository
        .save(new DashboardPermissionEntity(user.getUserToken(), dashboardId, false, true,
            true))));
    Date date = new Date();
    Set<User> team_list = new HashSet();
    for(int i =0; i < dashboardManifest.getTeam().size(); i++){
      team_list.add(userRepository.findByUserId(dashboardManifest.getTeam().get(i)));
    }
    return dashboardRepository.save(
        new Dashboard(dashboardId, dashboardManifest.getTitle(), description,
            dashboardManifest.getDefaultAssignedBy(), dashboardManifest.getCreatedBy(), date,
            date, team_list));
  }

  public List<Dashboard> getAll() {
    return dashboardRepository.findAll();
  }

  public Dashboard find(String dashboardId) {
    return dashboardRepository.findByDashboardId(dashboardId);
  }

  String getDefaultAssignedBy(String dashboardId) {
    return find(dashboardId).getDefaultAssignedBy();
  }

  public Dashboard update(String token, DashboardUpdateManifest dashboardUpdateManifest) {

    @NonNull String dashboardId = dashboardUpdateManifest.getDashboardId();

    tokenPermissionsService
        .validateToken(new Token(token).shouldHaveUpdateDashboardPermissions(dashboardId));
    Optional.ofNullable(dashboardUpdateManifest.getTitle())
        .ifPresent(v -> dashboardRepository.updateTitle(dashboardId, v));
    Optional.ofNullable(dashboardUpdateManifest.getDescription())
        .ifPresent(v -> dashboardRepository.updateDescription(dashboardId, v));
    Optional.ofNullable(dashboardUpdateManifest.getDefaultAssignedBy())
        .ifPresent(v -> dashboardRepository.updateDefaultAssignedBy(dashboardId, v));
    return dashboardRepository.findByDashboardId(dashboardId);
  }
}
