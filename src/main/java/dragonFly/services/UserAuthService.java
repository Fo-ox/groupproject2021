package dragonFly.services;

import dragonFly.entities.UserAuth;
import dragonFly.repositories.UserAuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class UserAuthService {

    private final UserAuthRepository userRepository;

    public List<UserAuth> getAllUsers() {
        return userRepository.findAll();
    }

    public String createUser(int userId, String login, String password) {
        String userToken = String.format("bear-%s", UUID.randomUUID());
        return userRepository.save(new UserAuth(userId, login, password, userToken)).getUserToken();
    }

}
