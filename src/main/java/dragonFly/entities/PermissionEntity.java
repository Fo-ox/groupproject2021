package dragonFly.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_permissions")
public class PermissionEntity {
    @Id
    @Column(name = "token")
    private String token;
    @Column(name = "is_allow_create_users")
    private Boolean isAllowCreateUsers;
    @Column(name = "is_allow_create_dashboards")
    private Boolean isAllowCreateDashboards;
}
