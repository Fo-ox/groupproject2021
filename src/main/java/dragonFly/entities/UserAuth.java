package dragonFly.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_auth")
public class UserAuth implements Serializable {
    @Id
    @Column(name = "user_id")
    private int userId;
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;
    @Column(name = "user_token")
    private String userToken;


    public UserAuth(int userId, String login, String password, String userToken) {
        this.userId = userId;
        this.login = login;
        this.password = password;
        this.userToken = userToken;
    }

}
