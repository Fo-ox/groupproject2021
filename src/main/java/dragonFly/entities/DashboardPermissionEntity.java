package dragonFly.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_dashboard_permissions")
@IdClass(DashboardPermissionEntity.DashboardPermissionId.class)
public class DashboardPermissionEntity {

  @Id
  @Column(name = "token")
  private String token;
  @Id
  @Column(name = "dashboard_id")
  private String dashboardId;
  @Column(name = "is_allow_update_dashboard")
  private Boolean isAllowUpdateDashboard;
  @Column(name = "is_allow_create_tasks")
  private Boolean isAllowCreateTasks;
  @Column(name = "is_allow_update_tasks")
  private Boolean isAllowUpdateTasks;

  @EqualsAndHashCode
  @NoArgsConstructor
  @AllArgsConstructor
  public static class DashboardPermissionId implements Serializable {

    private String token;
    private String dashboardId;
  }
}
