package dragonFly.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_comments")
public class Comment {
    @Id
    @Column(name = "comment_id")
    private String commentId;
    @Column(name = "task_id")
    private String taskId;
    @Column(name = "user_id")
    private String userId;
    @Column(name = "comment_text")
    private String commentText;
    @Column(name = "comment_time")
    private Date commentTime;
    @Column(name = "update_time")
    private Date updateTime;
    @Column(name = "deletion_time")
    private Date deletionTime;
}
