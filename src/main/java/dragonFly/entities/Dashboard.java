package dragonFly.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_dashboards")
public class Dashboard {
    @Id
    @Column(name = "dashboard_id")
    private String dashboardId;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Column(name = "default_assigned_by")
    private String defaultAssignedBy;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "creation_date")
    private Date creationDate;
    @Column(name = "modification_date")
    private Date modificationDate;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "prj_dashboard_users",
            joinColumns = @JoinColumn(name = "dashboard_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> team = new HashSet<>();
}
