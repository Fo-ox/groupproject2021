package dragonFly.entities;

import dragonFly.common.UserStatus;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_users")
public class User implements Serializable {

  @Id
  @Column(name = "user_id")
  private String userId;
  @Column(name = "login")
  private String login;
  @Column(name = "password")
  private String password;
  @Column(name = "user_token")
  private String userToken;
  @Column(name = "status")
  private String status;
  @Column(name = "first_name")
  private String firstName;
  @Column(name = "last_name")
  private String lastName;
  @Column(name = "profile_icon")
  private String profileIcon;

  public User(String userId, String login, String password, String userToken,
      String firstName, String lastName, String profileIcon) {
    this.userId = userId;
    this.login = login;
    this.password = password;
    this.userToken = userToken;
    this.status = UserStatus.ACTIVE.name();
    this.firstName = firstName;
    this.lastName = lastName;
    this.profileIcon = profileIcon;
  }
}
