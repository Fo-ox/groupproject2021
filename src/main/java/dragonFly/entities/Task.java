package dragonFly.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_tasks")
public class Task {
    @Id
    @Column(name = "task_id")
    private String taskId;
    @Column(name = "dashboard_id")
    private String dashboardId;
    @Column(name = "dashboard_seq")
    private String dashboardSeq;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "assigned_by")
    private String assignedBy;
    @Column(name = "status")
    private String status;
    @Column(name = "priority")
    private String priority;
    @Column(name = "type")
    private String type;
    @Column(name = "creation_date")
    private Date creationDate;
    @Column(name = "modification_date")
    private Date modificationDate;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "task_id")
    private List<Comment> commentList;

    public Task(String taskId, String dashboardId, String title, String description, String createdBy, String assignedBy, String status, String priority, String type, Date creationDate, Date modificationDate) {
        this.taskId = taskId;
        this.dashboardId = dashboardId;
        this.title = title;
        this.description = description;
        this.createdBy = createdBy;
        this.assignedBy = assignedBy;
        this.status = status;
        this.priority = priority;
        this.type = type;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }
}
