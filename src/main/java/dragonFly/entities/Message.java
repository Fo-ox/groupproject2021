package dragonFly.entities;

import liquibase.pro.packaged.S;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_messages")
public class Message {
    @Id
    @Column(name = "message_id")
    private String messageId;

    @Column(name = "message_text")
    private String messageText;

    @Column(name = "chat_id")
    private String chatId;

    @Column(name = "from_user")
    private String fromUserId;

    @Column(name = "message_time")
    private Date messageTime;

    @Column(name = "update_time")
    private Date updateTime;
}
