package dragonFly.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import liquibase.pro.packaged.S;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "prj_chats")
public class Chat implements Serializable {

    @Id
    @Column(name = "chat_id")
    private String chartId;

    @Column(name = "title")
    private String titleChat;

    @Column(name = "creation_time")
    private Date createTime;

    @Column(name = "deletion_time")
    private Date deletionTime;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "chat_icon")
    private String chatIcon;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "prj_chat_users",
            joinColumns = @JoinColumn(name = "chat_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users = new ArrayList<>();

    public Chat(String titleChat ) {
        this.titleChat = titleChat;
        this.createTime = new java.sql.Timestamp(new java.util.Date().getTime());
    }

    public void addUsers(List<User> users){
        this.users.addAll(users);
    }
}
