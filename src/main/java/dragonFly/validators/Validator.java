package dragonFly.validators;

import fj.data.Either;

public interface Validator<T> {
    Either<String,T> validate();
}
