package dragonFly.common;

import dragonFly.errors.DragonFlyException;
import dragonFly.services.TokenPermissionsService;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import lombok.Getter;

@Getter
public class Token {

  private final String tokenId;
  private final List<Function<TokenPermissionsService, String>> permissionChecks = new ArrayList();

  public Token(String tokenId) {
    this.tokenId = tokenId;
    if (tokenId == null) {
      throw new DragonFlyException("Token is null");
    }
    permissionChecks.add((v) -> v.isTokenExist(this));
    permissionChecks.add((v) -> v.isTokenActive(this));
  }

  public Token shouldHaveCreateUsersPermissions() {
    permissionChecks.add((v) -> v.isHaveCreateUsersPermissions(this));
    return this;
  }

  public Token shouldHaveCreateDashboardPermissions() {
    permissionChecks.add((v) -> v.isHaveCreateDashboardsPermissions(this));
    return this;
  }

  public Token shouldHaveUpdateDashboardPermissions(String dashboardId) {
    permissionChecks.add((v) -> v.isHaveUpdateDashboardPermission(this, dashboardId));
    return this;
  }

  public Token shouldHaveCreateTaskPermissions(String dashboardId) {
    permissionChecks.add((v) -> v.isHaveCreateTasksPermissions(this, dashboardId));
    return this;
  }

  public Token shouldHaveUpdateTaskPermissions(String dashboardId) {
    permissionChecks.add((v) -> v.isHaveUpdateTasksPermissions(this, dashboardId));
    return this;
  }

}
