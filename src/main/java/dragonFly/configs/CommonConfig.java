package dragonFly.configs;

import dragonFly.repositories.DashboardPermissionRepository;
import dragonFly.repositories.DashboardRepository;
import dragonFly.repositories.DragonFlyDashboardPermissionRepository;
import dragonFly.repositories.DragonFlyPermissionRepository;
import dragonFly.repositories.DragonFlyTokenRepository;
import dragonFly.repositories.PermissionRepository;
import dragonFly.repositories.TaskRepository;
import dragonFly.repositories.TokenRepository;
import dragonFly.repositories.UserRepository;
import dragonFly.services.DashboardService;
import dragonFly.services.TaskService;
import dragonFly.services.TokenPermissionsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonConfig {

  @Bean
  public DragonFlyTokenRepository getDragonFlyTokenRepository(TokenRepository tokenRepository) {
    return new DragonFlyTokenRepository(tokenRepository);
  }

  @Bean
  public DragonFlyPermissionRepository getDragonFlyPermissionRepository(
      PermissionRepository permissionRepository) {
    return new DragonFlyPermissionRepository(permissionRepository);
  }

  @Bean
  public DragonFlyDashboardPermissionRepository getDragonFlyDashboardPermissionRepository(
      DashboardPermissionRepository dashboardPermissionRepository) {
    return new DragonFlyDashboardPermissionRepository(dashboardPermissionRepository);
  }

  @Bean
  public TokenPermissionsService getTokenPermissionService(
      DragonFlyTokenRepository dragonFlyTokenRepository,
      DragonFlyPermissionRepository dragonFlyPermissionRepository,
      DragonFlyDashboardPermissionRepository dashboardPermissionRepository) {
    return new TokenPermissionsService(dragonFlyTokenRepository, dragonFlyPermissionRepository,
        dashboardPermissionRepository);
  }

  @Bean
  public DashboardService getDashboardService(DashboardRepository dashboardRepository,
      UserRepository userRepository,
      TokenPermissionsService tokenPermissionsService,
      DragonFlyDashboardPermissionRepository dashboardPermissionRepository) {
    return new DashboardService(dashboardRepository, userRepository, tokenPermissionsService,
        dashboardPermissionRepository);
  }

  @Bean
  public TaskService getTaskService(TaskRepository taskRepository,
      DashboardService dashboardService, TokenPermissionsService tokenPermissionsService) {
    return new TaskService(taskRepository, dashboardService, tokenPermissionsService);
  }

}
