package dragonFly.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UserDto {
    String userId;
    String firstName;
    String lastName;
    String profileIcon;
}
