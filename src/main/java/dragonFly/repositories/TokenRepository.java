package dragonFly.repositories;

import dragonFly.entities.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<TokenEntity,String> {

    TokenEntity findByToken(String token);

    TokenEntity save(TokenEntity token);
}
