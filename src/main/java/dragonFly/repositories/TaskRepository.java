package dragonFly.repositories;

import dragonFly.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task,String> {

    List<Task> findAll();

    List<Task> findByDashboardId(String dashboardId);

    Task findByTaskId(String taskId);

    Task save(Task task);

    @Transactional
    @Modifying
    @Query("UPDATE Task t SET t.title = :title WHERE task_id = :taskId")
    void updateTitle(@Param("taskId") String taskId, @Param("title") String title);

    @Transactional
    @Modifying
    @Query("UPDATE Task t SET t.description = :description WHERE task_id = :taskId")
    void updateDescription(@Param("taskId") String taskId,@Param("description") String description);

    @Transactional
    @Modifying
    @Query("UPDATE Task t SET t.assignedBy = :assignedBy WHERE task_id = :taskId")
    void updateAssignedBy(@Param("taskId") String taskId,@Param("assignedBy") String assignedBy);

    @Transactional
    @Modifying
    @Query("UPDATE Task t SET t.status = :status WHERE task_id = :taskId")
    void updateStatus(@Param("taskId") String taskId,@Param("status") String status);

    @Transactional
    @Modifying
    @Query("UPDATE Task t SET t.priority = :priority WHERE task_id = :taskId")
    void updatePriority(@Param("taskId") String taskId,@Param("priority") String priority);

    @Transactional
    @Modifying
    @Query("UPDATE Task t SET t.type = :type WHERE task_id = :taskId")
    void updateType(@Param("taskId") String taskId,@Param("type") String type);

    @Transactional
    @Modifying
    @Query("UPDATE Task t SET t.modificationDate = :modificationDate WHERE task_id = :taskId")
    void updateModificationDate(@Param("taskId") String taskId, @Param("modificationDate") Date modificationDate);
}
