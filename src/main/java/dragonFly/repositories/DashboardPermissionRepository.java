package dragonFly.repositories;

import dragonFly.entities.DashboardPermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardPermissionRepository extends JpaRepository<DashboardPermissionEntity, DashboardPermissionEntity.DashboardPermissionId> {
    DashboardPermissionEntity findByTokenAndDashboardId(String token, String dashboardId);

    DashboardPermissionEntity save(DashboardPermissionEntity dashboardPermissionEntity);
}
