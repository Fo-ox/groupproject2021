package dragonFly.repositories;

import dragonFly.entities.Dashboard;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DashboardRepository extends JpaRepository<Dashboard, String> {

  List<Dashboard> findAll();

  Dashboard findByDashboardId(String dashboardId);

  Dashboard save(Dashboard dashboard);

  @Transactional
  @Modifying
  @Query("UPDATE Dashboard t SET t.title = :title WHERE dashboard_id = :dashboardId")
  void updateTitle(@Param("dashboardId") String dashboardId, @Param("title") String title);

  @Transactional
  @Modifying
  @Query("UPDATE Dashboard t SET t.description = :description WHERE dashboard_id = :dashboardId")
  void updateDescription(@Param("dashboardId") String dashboardId,
      @Param("description") String description);

  @Transactional
  @Modifying
  @Query("UPDATE Dashboard t SET t.defaultAssignedBy = :defaultAssignedBy WHERE dashboard_id = :dashboardId")
  void updateDefaultAssignedBy(@Param("dashboardId") String dashboardId,
      @Param("defaultAssignedBy") String defaultAssignedBy);
}
