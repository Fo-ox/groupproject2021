package dragonFly.repositories;

import dragonFly.entities.TokenEntity;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class DragonFlyTokenRepository {

  private final TokenRepository tokenRepository;

  public Optional<TokenEntity> findByToken(String tokenId) {
    try {
      return Optional.of(tokenRepository.findByToken(tokenId));
    } catch (RuntimeException e) {
      log.error(e.getMessage());
      return Optional.empty();
    }
  }

  public TokenEntity save(TokenEntity token) {
    return tokenRepository.save(token);
  }

}
