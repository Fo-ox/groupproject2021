package dragonFly.repositories;

import dragonFly.entities.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAuthRepository extends JpaRepository<UserAuth, String> {

    List<UserAuth> findAll();

    UserAuth findByUserId(int userId);

    UserAuth findByLoginAndPassword(String user, String password);

    UserAuth save(UserAuth user);

}
