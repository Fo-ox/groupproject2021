package dragonFly.repositories;

import dragonFly.entities.PermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepository extends JpaRepository<PermissionEntity,String> {
    PermissionEntity findByToken(String token);

    PermissionEntity save(PermissionEntity permissionEntity);
}
