package dragonFly.repositories;

import dragonFly.entities.User;
import liquibase.pro.packaged.S;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    List<User> findAll();

    User findByUserId(String userId);

    User findByLoginAndPassword(String login, String password);

    User findByLogin(String login);

    User save(User user);

    User findByUserToken(String userToken);
}
