package dragonFly.repositories;

import dragonFly.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, String> {
    List<Message> findAll();

    String queryFindLastmessageByChatId = "SELECT * FROM prj_messages WHERE chat_id = :chat_id and message_id=(SELECT max(message_id) FROM prj_messages);";

    String queryFindmessagesByChatId = "SELECT * FROM prj_messages WHERE chat_id = :chat_id";

    @Query(value = queryFindLastmessageByChatId, nativeQuery = true)
    Message findMessageByChatId(@Param("chat_id") String chatId);

    @Query(value = queryFindmessagesByChatId, nativeQuery = true)
    List<Message> findMessagesByChatId(@Param("chat_id") String chatId);

    Message save(Message message);
}