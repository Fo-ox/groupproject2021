package dragonFly.repositories;

import dragonFly.entities.DashboardPermissionEntity;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class DragonFlyDashboardPermissionRepository {

  private final DashboardPermissionRepository dashboardPermissionRepository;

  public Optional<DashboardPermissionEntity> findByTokenAndDashboardId(String token,
      String dashboardId) {
    try {
      return Optional
          .of(dashboardPermissionRepository.findByTokenAndDashboardId(token, dashboardId));
    } catch (RuntimeException e) {
      log.error(e.getMessage());
      return Optional.empty();
    }
  }

  public DashboardPermissionEntity save(DashboardPermissionEntity dashboardPermissionEntity) {
    return dashboardPermissionRepository.save(dashboardPermissionEntity);
  }
}