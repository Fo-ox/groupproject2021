package dragonFly.repositories;

import dragonFly.entities.PermissionEntity;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class DragonFlyPermissionRepository {

  private final PermissionRepository permissionRepository;


  public Optional<PermissionEntity> findByToken(String token) {
    try {
      return Optional.of(permissionRepository.findByToken(token));
    } catch (RuntimeException e) {
      log.error(e.getMessage());
      return Optional.empty();
    }
  }

  PermissionEntity save(PermissionEntity permissionEntity) {
    return permissionRepository.save(permissionEntity);
  }


}
