package dragonFly.repositories;

import dragonFly.entities.Chat;
import dragonFly.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatsRepository extends JpaRepository<Chat, String> {
    List<Chat> findAll();

    String queryFindChartByChatId = "select * from prj_chats\n" +
            "where chat_id = :chat_id ";

    @Query(value = queryFindChartByChatId, nativeQuery = true)
    Chat findChartByTitleChatId(@Param("chat_id") String chatId);

    Chat save(Chat Chat);
}

