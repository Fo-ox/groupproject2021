package dragonFly.repositories;

import dragonFly.entities.Comment;
import dragonFly.entities.DashboardPermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment,String> {
    List<Comment> findByTaskId(String taskId);

    Comment save(Comment comment);
}