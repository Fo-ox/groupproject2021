package dragonFly.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardUpdateManifest {

  @NonNull
  private String dashboardId;
  private String title;
  private String description;
  private String defaultAssignedBy;
}
