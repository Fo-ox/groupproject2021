package dragonFly.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskUpdateManifest {
    @NonNull
    private String taskId;
    private String dashboardId;
    private String title;
    private String description;
    private String assignedBy;
    private String status;
    private String priority;
    private String type;
}