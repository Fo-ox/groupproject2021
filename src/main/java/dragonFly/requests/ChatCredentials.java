package dragonFly.requests;

import dragonFly.entities.User;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ChatCredentials {

  private String chat_id;
  private String title;
  private List<User> users;
  private String userId;
}
