package dragonFly.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardManifest {
    @NonNull
    private String title;
    private String description;
    @NonNull
    private String defaultAssignedBy;
    @NonNull
    private String createdBy;
    private List<String> team;
}
