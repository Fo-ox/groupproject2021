package dragonFly.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskManifest {
    @NonNull
    private String dashboardId;
    @NonNull
    private String title;
    private String description;
    @NonNull
    private String createdBy;
    private String assignedBy;
    private String status;
    private String priority;
    @NonNull
    private String type;
}
